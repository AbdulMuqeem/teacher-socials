//
//  InstituteCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 10/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class ActivitiesCellEvent : UITableViewCell {

    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblAttended:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
