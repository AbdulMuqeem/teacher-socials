//
//  ExpenseCell.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/12/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class ExpenseCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAmount:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
