//
//  EventsCell.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 25/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class EventsCell: UITableViewCell {

    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    @IBOutlet weak var imgLock:UIImageView!
    @IBOutlet weak var shadowView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
