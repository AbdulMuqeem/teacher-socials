//
//  WellBeingCell.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 25/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class WellBeingCell: UITableViewCell {

    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var shadowView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
