//
//  AlertView.swift
//  Food
//
//  Created by Abdul Muqeem on 12/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class TermsView : UIView {
    
    @IBOutlet weak var txtTerms:UITextView!
    @IBOutlet weak var btnClose:UIButton!
    
    var view: UIView!
    var delegate:SuccessViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    
    func xibSetup(){
        
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    
    func loadViewFromNib() -> UIView{
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
        
    }
    
    
    @IBAction func CloseAction(_ sender : UIButton) {
        
        self.delegate?.doneAction()
        
    }
    
    func alertShow(title:NSAttributedString) {
        
        self.txtTerms.attributedText = title
        
    }
    
}
