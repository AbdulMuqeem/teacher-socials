//
//  OffersHeadersCell.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 07/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class OffersHeadersCell: UICollectionViewCell {

    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var lblText:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
