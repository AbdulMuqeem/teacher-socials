//
//  LoginViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 30/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

extension LoginViewController: AlertViewDelegate , SuccessViewDelegate {
   
    func doneAction() {
        self.termsView.isHidden = true
    }
    
    func okAction() {
        
        if self.textAlert == "otp" {
            let vc = ActivateAccountViewController.instantiateFromStoryboard()
            vc.email = self.txtEmail.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if self.textAlert == "fb" {
            let vc = SignUpViewController.instantiateFromStoryboard()
            vc.isSocialLogin = true
            vc.socialObj = self.socialObj
            vc.Data = self.Data
            vc.type = "fb"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if self.textAlert == "google" {
            let vc = SignUpViewController.instantiateFromStoryboard()
            vc.isSocialLogin = true
            vc.socialObj = self.socialObj
            vc.Data = self.Data
            vc.type = "google"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.alertView.isHidden = true
    }
    
}

class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    var deviceId:String = ""
    var fbAccessToken:String = ""
    var googleAccessToken:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    var socialObj:SocialLogin?
    var Data = [String: Any]()
    var fbName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        
        SocialManager.GoogleSetDelegate(delegate: self)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        self.lblOne.text = "Hello Again..\nTeacher Socials"
        self.lblTwo.text = "The Largest Teachers Community\nin the region"
        self.lblThree.text = "Please Enter\nyour Log In Details"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
    }
    
    @IBAction func ForgotPasswordAction(_ sender : UIButton) {
        let vc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func SignupAction(_ sender : UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
            }
            
        })
       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
    
    @IBAction func googleAction(_ sender : UIButton) {
        SocialManager.GoogleLogin()
    }
    
    @IBAction func loginAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let password = self.txtPassword.text!
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , style: .warning)
            return
        }
        
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["emailId":email , "password":password]
        print(params)
        
        UserServices.Login(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let otp = response!["data"]["showOtp"].bool {
                if otp == true {
                    if let msg = response!["data"]["successMessage"].string {
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg , id: 0)
                        self.alertView.isHidden = false
                        self.textAlert = "otp"
                        return
                    }
                }
            }
            
            if let verify = response!["data"]["teacher"]["isVerified"].bool {
                if verify == false {
                    UserDefaults.standard.set("false", forKey: VERIFIED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: VERIFIED)
                }
            }
            
            if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                if submitted == false {
                    UserDefaults.standard.set("false", forKey: SUBMITTED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBMITTED)
                }
            }
            
            if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                if subscribe == false {
                    UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                }
            }
            
            
            let userResultObj = User(object:(response?["data"]["teacher"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let vc = HomeOffersViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
}

//MARK:- Facebook Login
extension LoginViewController {
    
    @IBAction func facebookAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if let udid = UIDevice.current.identifierForVendor?.uuidString {
            self.deviceId = udid
            print("Device Id: \(deviceId)")
            
        }
        
        SocialManager.FacebookLogin(from: self, permisions: [ "public_profile", "email" ]) { (result, error) in
            
            if error != nil {
                let error = String(describing: (error as AnyObject).localizedDescription)
                print("Error: \(error)")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.fbAccessToken = result!.token!.tokenString
            
            let params: [String: Any] = ["access_token": self.fbAccessToken , "deviceToken": self.deviceId]
            print(params)
            
            UserServices.FacebookLogin(param: params, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        let error = String(describing: (error as AnyObject).localizedDescription)
                        print("Error: \(error)")
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                        self.alertView.isHidden = false
                        self.stopLoading()
                        return
                    }
                    let msg = response?["errorMessage"].stringValue
                    print("Message: \(String(describing: msg))")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                print(response!)
                
                self.socialObj = SocialLogin(object:(response?["data"])!)
                
                if let redirect = self.socialObj!.redirectForm {
                    
                    if redirect == false {
                        
                        self.stopLoading()
                        
                        if let verify = response!["data"]["teacher"]["isVerified"].bool {
                            if verify == false {
                                UserDefaults.standard.set("false", forKey: VERIFIED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: VERIFIED)
                            }
                        }
                        
                        if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                            if submitted == false {
                                UserDefaults.standard.set("false", forKey: SUBMITTED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBMITTED)
                            }
                        }
                        
                        if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                            if subscribe == false {
                                UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                            }
                        }
                        
                        
                        let userResultObj = User(object:(response?["data"]["teacher"])!)
                        UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                        Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                        
                        let vc = HomeOffersViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        
                        self.getFBData()
                        
                    }
                }
            })
        }
    }
    
    func getFBData() {
        
        SocialManager.FacebookGetUser(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email "], callback: { (connection, result, error) in
            
            if error != nil {
                let error = String(describing: (error as AnyObject).localizedDescription)
                print("Error: \(error)")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            guard let userInfo = result as? [String: Any] else {
                return
            } //handle the error
            
            if let firstName = (userInfo["first_name"]) as? String {
                print(firstName)
                self.fbName = firstName
                self.Data["name"] = self.fbName
            }
            
            if let lastName = (userInfo["last_name"]) as? String {
                print(lastName)
                self.fbName = self.fbName + " " + lastName
                self.Data["name"] = self.fbName
            }
            
            if let id = (userInfo["id"]) as? String {
                print(id)
                self.Data["id"] = id
            }
            
            if let email = (userInfo["email"]) as? String {
                print(email)
                self.Data["email"] = email
            }
            
            self.Data["token"] = self.fbAccessToken
            self.Data["deviceId"] = self.deviceId
            
            self.stopLoading()
            
            if let msg = self.socialObj!.successMessage {
                self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Success!", msg: msg , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "fb"
            }
            
        })
        
    }
    
}

//MARK:- Google Login
extension LoginViewController :  GIDSignInDelegate {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil {
            let error = String(describing: (error as AnyObject).localizedDescription)
            print("Error: \(error)")
            self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
            self.alertView.isHidden = false
            self.stopLoading()
            return
        }
        else {
            // Perform any operations on signed in user here.
            let userId = user.userID
            let idToken = user.authentication.idToken
            let access_token = user.authentication.accessToken
            let fullName = user.profile.name
            let email = user.profile.email
                   
            
            print("Access Token: \(access_token!)")
            print("User ID: \(userId!)")
            print("ID Token: \(idToken!)")
            
            if let udid = UIDevice.current.identifierForVendor?.uuidString {
                self.deviceId = udid
                print("Device Id: \(deviceId)")
                
            }
            
            self.googleAccessToken = access_token!
            
            let params: [String: Any] = ["access_token": self.googleAccessToken , "deviceToken": self.deviceId]
            print(params)
            
            UserServices.GoogleLogin(param: params, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        let error = String(describing: (error as AnyObject).localizedDescription)
                        print("Error: \(error)")
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                        self.alertView.isHidden = false
                        self.stopLoading()
                        return
                    }
                    let msg = response?["errorMessage"].stringValue
                    print("Message: \(String(describing: msg))")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                print(response!)
                self.socialObj = SocialLogin(object:(response?["data"])!)
                
                if let redirect = self.socialObj!.redirectForm {
                    
                    if redirect == false {
                        
                        self.stopLoading()
                        
                        if let verify = response!["data"]["teacher"]["isVerified"].bool {
                            if verify == false {
                                UserDefaults.standard.set("false", forKey: VERIFIED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: VERIFIED)
                            }
                        }
                        
                        if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                            if submitted == false {
                                UserDefaults.standard.set("false", forKey: SUBMITTED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBMITTED)
                            }
                        }
                        
                        if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                            if subscribe == false {
                                UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                            }
                        }
                        
                        let userResultObj = User(object:(response?["data"]["teacher"])!)
                        UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                        Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                        
                        let vc = HomeOffersViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        
                        self.Data["name"] = fullName!.capitalizingFirstLetter()
                        self.Data["id"] = idToken
                        self.Data["email"] = email
                        self.Data["token"] = self.googleAccessToken
                        self.Data["deviceId"] = self.deviceId
                        
                        self.stopLoading()
                        
                        if let msg = self.socialObj!.successMessage {
                            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Success!", msg: msg , id: 0)
                            self.alertView.isHidden = false
                            self.textAlert = "google"
                        }
                    }
                }
                
                return
            })
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
        print("Error: \(error.localizedDescription)")
    }
    
}

