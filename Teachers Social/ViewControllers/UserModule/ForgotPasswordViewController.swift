//
//  ForgotPasswordViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 31/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ForgotPasswordViewController: AlertViewDelegate , SuccessViewDelegate {
    
    func doneAction() {
        self.termsView.isHidden = true
    }
    
    func okAction() {
        
        if self.textAlert == "otp" {
            let vc = ResetPasswordViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.alertView.isHidden = true
    }
    
}

class ForgotPasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var txtEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        
        self.lblOne.text = "Forgot Your\nPassword"
        self.lblTwo.text = "No Problem.."
        self.lblThree.text = "Please Enter your email\nto reset your password"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func rememberPasswordAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignupAction(_ sender : UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
    
    
    @IBAction func sendAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["emailId":email]
        print(params)
        
        UserServices.ForgotPassword(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let msg = response!["data"]["successMessage"].string {
                self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: msg , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "otp"
            }
            
        })        
    }
}
