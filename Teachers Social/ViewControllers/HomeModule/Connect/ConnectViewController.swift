//
//  ConnectViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown

extension ConnectViewController: AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        
        //        if self.isFromSettings == false {
        //
        //            for controller in self.navigationController!.viewControllers as Array {
        //                if controller.isKind(of: HomeOffersViewController.self) {
        //                    self.navigationController!.popToViewController(controller, animated: false)
        //                    break
        //                }
        //            }
        //
        //        }
        //        else {
        
        if self.isFromSettings == false {
            self.txtDescription.text = ""
            self.successView.isHidden = true
        }
        else {

            if self.textAlert == "close" {
                
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                Singleton.sharedInstance.CurrentUser = nil
                
                let nav = RootViewController.instantiateFromStoryboard()
                if #available(iOS 13, *) {
                    SceneDelegate.getInstatnce().window?.rootViewController = nav
                } else {
                    AppDelegate.getInstatnce().window?.rootViewController = nav
                }
                let VC = LoginViewController.instantiateFromStoryboard()
                nav.pushViewController(VC, animated: true)
                
            }
            else {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ProfileViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
            }
        }
    }
    
}

class ConnectViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ConnectViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ConnectViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var tabBar:UITabBar!
    @IBOutlet weak var typeView:UIView!
    @IBOutlet weak var txtType:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    var typeDropDown = DropDown()
    
    var isFromSettings = false
    var settingType = ""
    var itemIndex = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = "CONNECT"
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.txtDescription.placeholder = "Enter Description"
        
        // Gender DropDown
        self.typeDropDown.anchorView = self.typeView // UIView or UIBarButtonItem
        self.typeDropDown.bottomOffset = CGPoint(x: 0 , y: (self.typeDropDown.anchorView?.plainView.bounds.height)!)
        self.typeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtType.text = item
            print("Selected item: \(item) at index: \(index)")
            
        }
        
        self.typeDropDown.dataSource = ["Become a member" , "Share an idea" , "Need some help" , "Feedback"]
        
        if self.isFromSettings == true {
            self.lblTitle.text = self.settingType.capitalized
            self.txtType.text = self.settingType
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.navigationController?.isNavigationBarHidden = true
        
        if self.isFromSettings == false {
            self.tabBar.selectedItem = self.tabBar.items![3] as UITabBarItem
        }
        else {
            self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        }
        
    }
    
    
    @IBAction func typeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        if self.isFromSettings == false {
            self.typeDropDown.show()
        }
        
    }
    
    func closeAccount() {
        
        self.view.endEditing(true)
        
        let type = self.txtType.text!
        let description = self.txtDescription.text!
        
        if type.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select type" , style: .warning)
            return
        }
        
        if description.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter description" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let email = Singleton.sharedInstance.CurrentUser!.emailId!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["feedbackSubject": type , "feedbackText" : description , "emailId": email , "teacherId": id]
        print(params)
        
        ProfileServices.AccountClosed(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
                self.textAlert = "close"
            }
            
        })
        
    }
    
    @IBAction func sendAction(_ sender : UIButton) {
        
        if self.settingType == "Close Account" {
            self.closeAccount()
            return
        }
        
        self.view.endEditing(true)
        
        let type = self.txtType.text!
        let description = self.txtDescription.text!
        
        if type.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select type" , style: .warning)
            return
        }
        
        if description.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter description" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let email = Singleton.sharedInstance.CurrentUser!.emailId!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["feedbackSubject": type , "feedbackText" : description , "emailId": email , "teacherId": id]
        print(params)
        
        ProfileServices.Connect(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    @IBAction func openFBLink(_ sender : UIButton) {

        self.view.endEditing(true)
        
        guard let url = URL(string: "https://m.facebook.com/groups/686952178143566") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
    @IBAction func openInstagramLink(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.view.endEditing(true)
        guard let url = URL(string: "https://www.instagram.com/teachersocials/") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
}

extension ConnectViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        if self.isFromSettings == true {
            self.itemIndex = 4
        }
        
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            
            if self.isFromSettings == false {
                self.tabBar.selectedItem = self.tabBar.items![3] as UITabBarItem
                return
            }
            else {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ConnectViewController.self) {
                        self.viewDidLoad()
                        self.viewWillAppear(true)
                        self.isFromSettings = false
                        self.txtType.text = ""
                        self.settingType = ""
                        self.itemIndex = 3
                        self.tabBar.selectedItem = self.tabBar.items![3] as UITabBarItem
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                    else {
                        let vc = ConnectViewController.instantiateFromStoryboard()
                        self.isFromSettings = false
                        self.txtType.text = ""
                        self.settingType = ""
                        self.itemIndex = 3
                        self.tabBar.selectedItem = self.tabBar.items![3] as UITabBarItem
                        self.navigationController?.pushViewController(vc, animated: false)
                        break
                    }
                }
                
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
