//
//  SubscribedViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 27/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class SubscribedViewController: UIViewController {

    class func instantiateFromStoryboard() -> SubscribedViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubscribedViewController
    }
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblOne.text = "Enjoy Your Best Offers"
        self.lblTwo.text = "You have 365 days of\nunlimited offer"
        
        self.title = "My Subscription"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.LightStatusBar()
    
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func letsGoAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeOffersViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
