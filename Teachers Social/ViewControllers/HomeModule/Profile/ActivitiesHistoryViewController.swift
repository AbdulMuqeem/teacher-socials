//
//  ActivitiesHistoryViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 22/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ActivitiesHistoryViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class ActivitiesHistoryViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ActivitiesHistoryViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ActivitiesHistoryViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    var profileObj:ProfileDetails?
    
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    @IBOutlet weak var tblView:UITableView!
    var ActivitiesListArray:[ActivitiesHistory]? = [ActivitiesHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.initData()
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.getActivitiesHistory()
        
        //Register Cell
        let cellOffer = UINib(nibName:String(describing:ActivitiesCellOffers.self), bundle: nil)
        self.tblView.register(cellOffer, forCellReuseIdentifier: String(describing: ActivitiesCellOffers.self))
        
        let cellEvent = UINib(nibName:String(describing:ActivitiesCellEvent.self), bundle: nil)
        self.tblView.register(cellEvent, forCellReuseIdentifier: String(describing: ActivitiesCellEvent.self))
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initData() {
        
        if self.profileObj == nil {
            return
        }
        
        if let image = self.profileObj!.profilePicture {
            self.imgUser.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.profileObj!.name {
            self.lblName.text = "Hi " + name
        }
        
    }
    
    func getActivitiesHistory() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id]
        print(param)
        
        ProfileServices.GetMyActivities(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["activitiesHistory"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  ActivitiesHistory(json: resultObj)
                self.ActivitiesListArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
}

extension ActivitiesHistoryViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ActivitiesListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(120)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = self.ActivitiesListArray![indexPath.row]
        
        if obj.activityType == "Event" {
            
            let cell:ActivitiesCellEvent = tableView.dequeueReusableCell(withIdentifier:String(describing:ActivitiesCellEvent.self)) as!  ActivitiesCellEvent
            
            if let image = obj.eventPictureUrl {
                cell.imgLogo.setImageFromUrl(urlStr: image)
            }
            
            if let name = obj.title {
                cell.lblName.text = name
            }
            
            if let date = obj.eventTimestamp {
                cell.lblAddress.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "dd-MM- yyyy")
                
            }
            
            if obj.isAttended == true {
                cell.lblAttended.isHidden = false
            }
            
            return cell
            
        }
        else {
            let cell:ActivitiesCellOffers = tableView.dequeueReusableCell(withIdentifier:String(describing:ActivitiesCellOffers.self)) as!  ActivitiesCellOffers
            
            if let image = obj.offerPictureUrl {
                cell.imgLogo.setImageFromUrl(urlStr: image)
            }
            
            if let name = obj.offerName {
                cell.lblName.text = name
            }
            
            if let date = obj.timestamp {
                cell.lblAddress.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "dd-MM- yyyy")
                
            }
            
            return cell
            
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        let obj = self.ActivitiesListArray![indexPath.row]
        
        if obj.activityType == "Event" {
            
            if obj.isAttended == false {
                let vc = DiscountViewController.instantiateFromStoryboard()
                vc.profileObj = self.profileObj
                vc.activityObj = obj
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let vc = DiscountSuccessfullViewController.instantiateFromStoryboard()
                vc.profileObj = self.profileObj
                vc.activitiesObj = obj
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else {
            
            let vc = DiscountSuccessfullViewController.instantiateFromStoryboard()
            vc.profileObj = self.profileObj
            vc.activitiesObj = obj
            vc.isOffer = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
}


extension ActivitiesHistoryViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}
