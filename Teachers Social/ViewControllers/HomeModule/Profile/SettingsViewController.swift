//
//  SettingsViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 05/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SettingsViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class SettingsViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SettingsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SettingsViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    var profileObj:ProfileDetails?
    
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var tabBar:UITabBar!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.title = "Settings"
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.initData()
        self.TabBarInit()
        self.tabBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initData() {
        
        if self.profileObj == nil {
            return
        }
        
        if let image = self.profileObj!.profilePicture {
            self.imgUser.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.profileObj!.name {
            self.lblName.text = "Hi " + name
        }
        
    }
    
    @IBAction func historyAction(_ sender : UIButton) {
        let vc = ActivitiesHistoryViewController.instantiateFromStoryboard()
        vc.profileObj = self.profileObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notificationAction(_ sender : UIButton) {
        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "will be implemented" , id: 0)
        self.alertView.isHidden = false
        return
    }
    
    @IBAction func helpAction(_ sender : UIButton) {
        let vc = ConnectViewController.instantiateFromStoryboard()
        vc.isFromSettings = true
        vc.settingType = "Need some help"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func memberAction(_ sender : UIButton) {
        let vc = ConnectViewController.instantiateFromStoryboard()
        vc.isFromSettings = true
        vc.settingType = "Become a member"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ideaAction(_ sender : UIButton) {
        let vc = ConnectViewController.instantiateFromStoryboard()
        vc.isFromSettings = true
        vc.settingType = "Share an idea"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func feedbackAction(_ sender : UIButton) {
        let vc = ConnectViewController.instantiateFromStoryboard()
        vc.isFromSettings = true
        vc.settingType = "Feedback"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        let vc = ConnectViewController.instantiateFromStoryboard()
        vc.isFromSettings = true
        vc.settingType = "Close Account"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SettingsViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}
