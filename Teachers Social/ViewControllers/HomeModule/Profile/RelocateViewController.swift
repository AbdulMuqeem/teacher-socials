//
//  RelocateViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 06/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension RelocateViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

class RelocateViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> RelocateViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RelocateViewController
    }
    
    @IBOutlet weak var txtCurrentLocation:UITextField!
    @IBOutlet weak var txtDesiredLocation:UITextField!
    @IBOutlet weak var txtreason:UITextView!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func sendAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let currentLocation = self.txtCurrentLocation.text!
        let desiredLocation = self.txtDesiredLocation.text!
        let reason = self.txtreason.text!
        
        if currentLocation.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter current location" , style: .warning)
            return
        }
        
        if desiredLocation.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter desired location" , style: .warning)
            return
        }
        
        if reason.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter reason" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let email = Singleton.sharedInstance.CurrentUser!.emailId!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["relocationFormReason": reason , "currentLocation" : currentLocation , "desiredLocation": desiredLocation , "postedBy": email , "postedById": id]
        print(params)
        
        ProfileServices.SubmitRelocation(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let msg = "Your relocation form has been\nsubmitted to Teacher Socials"
            self.successView.alertShow(title: "Great Job!", msg: msg)
            self.successView.isHidden = false
            
        })
        
    }
    
    @IBAction func openFBLink(_ sender : UIButton) {

        self.view.endEditing(true)
        
        guard let url = URL(string: "https://m.facebook.com/groups/686952178143566") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
    @IBAction func openInstagramLink(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.view.endEditing(true)
        guard let url = URL(string: "https://www.instagram.com/teachersocials/") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
}

extension RelocateViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}
