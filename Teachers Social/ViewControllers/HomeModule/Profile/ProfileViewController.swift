//
//  ProfileViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ALCameraViewController
import AWSS3
import AWSCore

extension ProfileViewController : AlertViewDelegate , SuccessViewDelegate , RefreshDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.successView.isHidden = true
    }
    
    func StartRefresh() {
        self.resetButton()
    }
}

class ProfileViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var tabBar:UITabBar!
    
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var btnSetting:UIButton!
    @IBOutlet weak var btnVerifyAccount:UIButton!
    @IBOutlet weak var btnSubscription:UIButton!
    @IBOutlet weak var btnInviteFriend:UIButton!
    @IBOutlet weak var btnTermsPolicies:UIButton!
    @IBOutlet weak var btnAboutUs:UIButton!
    @IBOutlet weak var btnRelocate:UIButton!
    @IBOutlet weak var btnExpenseManager:UIButton!
    @IBOutlet weak var btnHome:UIButton!
    @IBOutlet weak var btnLogout:UIButton!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var logoutView:UIView!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    var profileObj:ProfileDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.shadowView.isHidden = true
        self.logoutView.isHidden = true
        
        self.tabBar.isHidden = true
        self.getProfileDetails()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileDetails), name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.resetButton()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func getProfileDetails() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id]
        print(param)
        
        ProfileServices.GetProfile(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.profileObj = ProfileDetails(object:(response?["data"]["profileDetails"])!)
            
            if let image = self.profileObj!.profilePicture {
                self.imgUser.setImageFromUrl(urlStr: image)
            }
            
            if let name = self.profileObj!.name {
                self.lblName.text = "Hi " + name
            }
            
        })
        
    }
    
    @IBAction func MyProfileAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnProfile.BorderColor = UIColor.white
        self.btnProfile.backgroundColor = UIColor.white
        self.btnProfile.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = MyProfileViewController.instantiateFromStoryboard()
            vc.profileObj = self.profileObj
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func SettingAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnSetting.BorderColor = UIColor.white
        self.btnSetting.backgroundColor = UIColor.white
        self.btnSetting.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = SettingsViewController.instantiateFromStoryboard()
            vc.profileObj = self.profileObj
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func VerifyAccountAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnVerifyAccount.BorderColor = UIColor.white
        self.btnVerifyAccount.backgroundColor = UIColor.white
        self.btnVerifyAccount.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = VerifyAccountViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func MySubscriptionAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnSubscription.BorderColor = UIColor.white
        self.btnSubscription.backgroundColor = UIColor.white
        self.btnSubscription.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let subscription = UserDefaults.standard.object(forKey: SUBSCRIPTION) as? String
            
            if subscription == "false" {
                let vc = MySubscriptionViewController.instantiateFromStoryboard()
                vc.delegate = self
                vc.modalPresentationStyle = .custom
                self.present(vc, animated:true, completion: nil)
            }
            else {
                let vc = SubscribedViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func InviteFriendAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnInviteFriend.BorderColor = UIColor.white
        self.btnInviteFriend.backgroundColor = UIColor.white
        self.btnInviteFriend.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = InviteFriendViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func TermsAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnTermsPolicies.BorderColor = UIColor.white
        self.btnTermsPolicies.backgroundColor = UIColor.white
        self.btnTermsPolicies.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = TermsViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func AboutAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnAboutUs.BorderColor = UIColor.white
        self.btnAboutUs.backgroundColor = UIColor.white
        self.btnAboutUs.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = AboutUsViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func RelocateAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnRelocate.BorderColor = UIColor.white
        self.btnRelocate.backgroundColor = UIColor.white
        self.btnRelocate.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = RelocateViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func ExpenseAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnExpenseManager.BorderColor = UIColor.white
        self.btnExpenseManager.backgroundColor = UIColor.white
        self.btnExpenseManager.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            let vc = ExpenseManagerViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        self.resetButton()
        
    }
    
    @IBAction func HomeAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnHome.BorderColor = UIColor.white
        self.btnHome.backgroundColor = UIColor.white
        self.btnHome.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
            
        }
        
    }
    
    @IBAction func LogoutAction( _ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        self.btnLogout.BorderColor = UIColor.white
        self.btnLogout.backgroundColor = UIColor.white
        self.btnLogout.setTitleColor(UIColor.init(rgb: THEME_COLOR) , for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1 ) {
            
            self.view.endEditing(true)
            self.shadowView.isHidden = false
            self.logoutView.setView(view: self.logoutView, hidden: false)
        }
    }
    
    @IBAction func okAction(_ sender : UIButton) {
        
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.removeObject(forKey: VERIFIED)
        UserDefaults.standard.removeObject(forKey: SUBMITTED)
        UserDefaults.standard.removeObject(forKey: SUBSCRIPTION)
        UserDefaults.standard.synchronize()
        Singleton.sharedInstance.CurrentUser = nil
        
        if #available(iOS 13.0, *) {
            let nav = RootViewController.instantiateFromStoryboard()
            SceneDelegate.getInstatnce().window?.rootViewController = nav
            let VC = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(VC, animated: true)
        }
        else {
            let nav = RootViewController.instantiateFromStoryboard()
            AppDelegate.getInstatnce().window?.rootViewController = nav
            let VC = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(VC, animated: true)
        }
        
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.logoutView.setView(view: self.logoutView, hidden: true)
        self.resetButton()
    }
    
    @IBAction func cancelAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.logoutView.setView(view: self.logoutView, hidden: true)
        self.resetButton()
        
    }
    
}

// Image
extension ProfileViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func imageAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera()
        
    }
    
    func openCamera(){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            
            if let pickedImage = image {
                
                if let image = self?.fixOrientation(img: pickedImage) {
                    self?.imgUser.image = image
                } else {
                    self?.imgUser.image = pickedImage
                }
                
                self?.imgUser.contentMode = .scaleAspectFill
                self?.imgUser.clipsToBounds = true
                
            }
            
            self?.dismiss(animated: true, completion: nil)
            
            if image != nil {
                self?.uploadImageAWS(withImage: image!)
            }
        }
        
        present(cameraViewController, animated: true, completion: nil)
        
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    func uploadImageAWS(withImage image: UIImage) {
        
        self.startLoading(message: "")
        var imageURL:String = ""
        
        let credentials = AWSStaticCredentialsProvider(accessKey: AWS_ACCESS_KEY , secretKey: AWS_SECRET_KEY)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSouth1 , credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let s3BucketName = AWS_BUCKET_NAME
        let data: Data = image.jpegData(compressionQuality: 0.5)!
        let remoteName = generateRandomStringWithLength(length: 10) + "." + data.format
        print("REMOTE NAME : ",remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                // Update a progress bar
                print("Task: \(task)")
                print("Progress: \(progress)")
                
            })
        }
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                
                if let error = error as? String {
                    self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                self.stopLoading()
                self.uploadProfilePicture(imageName: imageURL)
                
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/" + data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            
            if let error = task.error as? String {
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
            }
            else {
                
                if task.result != nil {
                    
                    let url = AWSS3.default().configuration.endpoint.url
                    print("url: \(url!)")
                    
                    let publicURL =  url?.appendingPathComponent(AWS_BUCKET_NAME).appendingPathComponent(remoteName)
                    
                    if let absoluteString = publicURL?.absoluteString {
                        print("Image URL : ",absoluteString)
                        imageURL = absoluteString
                        
                    }
                }
            }
            
            return nil
            
        }
        
    }
    
    func uploadProfilePicture(imageName:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id , "profilePicture":imageName ]
        print(param)
        
        ProfileServices.UpdateProfileImage(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
                self.getProfileDetails()
            }
        })
    }
}

extension ProfileViewController {
    
    func resetButton() {
        
        self.btnProfile.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnProfile.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnProfile.setTitleColor(UIColor.white , for: .normal)
        
        self.btnSetting.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnSetting.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnSetting.setTitleColor(UIColor.white , for: .normal)
        
        self.btnVerifyAccount.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnVerifyAccount.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnVerifyAccount.setTitleColor(UIColor.white , for: .normal)
        
        self.btnSubscription.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnSubscription.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnSubscription.setTitleColor(UIColor.white , for: .normal)
        
        self.btnInviteFriend.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnInviteFriend.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnInviteFriend.setTitleColor(UIColor.white , for: .normal)
        
        self.btnTermsPolicies.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnTermsPolicies.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnTermsPolicies.setTitleColor(UIColor.white , for: .normal)
        
        self.btnAboutUs.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnAboutUs.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnAboutUs.setTitleColor(UIColor.white , for: .normal)
        
        self.btnRelocate.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnRelocate.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnRelocate.setTitleColor(UIColor.white , for: .normal)
        
        self.btnExpenseManager.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnExpenseManager.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnExpenseManager.setTitleColor(UIColor.white , for: .normal)
        
        self.btnLogout.BorderColor = UIColor.init(rgb: THEME_COLOR)
        self.btnLogout.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.btnLogout.setTitleColor(UIColor.white , for: .normal)

        
    }
}
