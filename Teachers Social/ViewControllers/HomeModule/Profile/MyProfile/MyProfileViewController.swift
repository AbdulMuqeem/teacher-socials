//
//  MyProfileViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 05/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown
import ActionSheetPicker_3_0
import ALCameraViewController
import AWSS3
import AWSCore

extension MyProfileViewController : AlertViewDelegate  , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}


class MyProfileViewController: UIViewController  {
    
    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var imgUser:UIImageView!
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtNationality:UITextField!
    @IBOutlet weak var txtGender:UITextField!
    @IBOutlet weak var txtBirthDate:UITextField!
    
    @IBOutlet weak var countryView:UIView!
    @IBOutlet weak var genderView:UIView!
    
    var countryName = [String]()
    var countriesDropDown = DropDown()
    var genderDropDown = DropDown()
    var profileObj:ProfileDetails?
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.initData()
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.CountriesList()
        
        // Countries Dropdown
        countriesDropDown.anchorView = self.countryView // UIView or UIBarButtonItem
        countriesDropDown.bottomOffset = CGPoint(x: 0 , y: (countriesDropDown.anchorView?.plainView.bounds.height)!)
        countriesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.txtNationality.text = item
            
        }
        
        // Gender DropDown
        genderDropDown.anchorView = self.genderView // UIView or UIBarButtonItem
        genderDropDown.bottomOffset = CGPoint(x: 0 , y: (genderDropDown.anchorView?.plainView.bounds.height)!)
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtGender.text = item
            print("Selected item: \(item) at index: \(index)")
            
        }
        
        self.genderDropDown.dataSource = ["Male" , "Female"]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    func initData() {
        
        if self.profileObj == nil {
            return
        }
        
        if let image = self.profileObj!.profilePicture {
            self.imgUser.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.profileObj!.name {
            self.lblName.text = "Hi " + name
            self.txtName.text = name
        }
        
        if let email = self.profileObj!.emailId {
            self.txtEmail.text = email
        }
        
        if let phone = self.profileObj!.contactNumber {
            self.txtPhone.text = phone
        }
        
        if let gender = self.profileObj!.gender {
            if gender == 0 {
                self.txtGender.text = ""
            }
            else if gender == 1 {
                self.txtGender.text = "Male"
            }
            else if gender == 2 {
                self.txtGender.text = "Female"
            }
        }
        
        if let country = self.profileObj!.countryOfOrigin {
            self.txtNationality.text = country
        }
        
        if let dob = self.profileObj!.dateOfBirth {
            if dob == "1900-01-01T00:00:00.000Z" {
                self.txtBirthDate.text = ""
                return
            }
            self.txtBirthDate.text = dob.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "MMM dd yyyy")
        }
        
    }
    
    @IBAction func countryAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.countriesDropDown.show()
        
    }
    
    func CountriesList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.Country(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let obj =  Countries(json: response!["data"])
            self.countryName = obj.nationalities!
            self.countriesDropDown.dataSource = self.countryName
            
        })
        
    }
    
    
    @IBAction func dobAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Select DOB", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: Date(), doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtBirthDate.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func genderAction(_ sender:UIButton) {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.genderDropDown.show()
        }
        
    }
    
    @IBAction func smartPassAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        let vc = ChangeSmartPassViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func PasswordAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        let vc = ChangePasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ConfirmAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let name = self.txtName.text!
        let phone = self.txtPhone.text!
        let gender = self.txtGender.text!
        let dob = self.txtBirthDate.text!
        let country = self.txtNationality.text!
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        var genderType:Int!
        
        if gender == "Male" {
            genderType = 1
        } else if gender == "Female" {
            genderType = 2
        }
        else {
            genderType = 0
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id , "name" : name , "contactNumber":phone , "gender":genderType! , "dateOfBirth":dob , "countryOfOrigin":country]
        print(param)
        
        ProfileServices.UpdateProfile(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
            }
            
        })
        
    }
    
}


// Image
extension MyProfileViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func imageAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera()
        
    }
    
    func openCamera(){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            
            if let pickedImage = image {
                
                if let image = self?.fixOrientation(img: pickedImage) {
                    self?.imgUser.image = image
                } else {
                    self?.imgUser.image = pickedImage
                }
                
                self?.imgUser.contentMode = .scaleAspectFill
                self?.imgUser.clipsToBounds = true
                
            }
            
            self?.dismiss(animated: true, completion: nil)
            
            if image != nil {
                self?.uploadImageAWS(withImage: image!)
            }
        }
        
        present(cameraViewController, animated: true, completion: nil)
        
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    func uploadImageAWS(withImage image: UIImage) {
        
        self.startLoading(message: "")
        var imageURL:String = ""
        
        let credentials = AWSStaticCredentialsProvider(accessKey: AWS_ACCESS_KEY , secretKey: AWS_SECRET_KEY)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSouth1 , credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let s3BucketName = AWS_BUCKET_NAME
        let data: Data = image.jpegData(compressionQuality: 0.5)!
        let remoteName = generateRandomStringWithLength(length: 10) + "." + data.format
        print("REMOTE NAME : ",remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                // Update a progress bar
                print("Task: \(task)")
                print("Progress: \(progress)")
                
            })
        }
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                
                if let error = error as? String {
                    self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                self.stopLoading()
                self.uploadProfilePicture(imageName: imageURL)
                
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/" + data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            
            if let error = task.error as? String {
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
            }
            else {
                
                if task.result != nil {
                    
                    let url = AWSS3.default().configuration.endpoint.url
                    print("url: \(url!)")
                    
                    let publicURL =  url?.appendingPathComponent(AWS_BUCKET_NAME).appendingPathComponent(remoteName)
                    
                    if let absoluteString = publicURL?.absoluteString {
                        print("Image URL : ",absoluteString)
                        imageURL = absoluteString
                        
                    }
                }
            }
            
            return nil
            
        }
        
    }
    
    func uploadProfilePicture(imageName:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id , "profilePicture":imageName ]
        print(param)
        
        ProfileServices.UpdateProfileImage(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
            }
        })
    }
}

extension MyProfileViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}


