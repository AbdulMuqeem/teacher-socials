//
//  ChangeSmartPassViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 05/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ChangeSmartPassViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}


class ChangeSmartPassViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ChangeSmartPassViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChangeSmartPassViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtOldPassword:UITextField!
    @IBOutlet weak var txtNewPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.txtOldPassword.delegate = self
        self.txtNewPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        self.txtOldPassword.keyboardType = .numberPad
        self.txtNewPassword.keyboardType = .numberPad
        self.txtConfirmPassword.keyboardType = .numberPad

        
        self.title = "Change Smart Pass"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.LightStatusBar()
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let oldPassword = self.txtOldPassword.text!
        let password = self.txtNewPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if oldPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter old smart pass" , style: .warning)
            return
        }
        
        if password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter new smart pass" , style: .warning)
            return
        }
        
        if confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter confirm smart passw" , style: .warning)
            return
        }
        
        if oldPassword.count != 4 || password.count != 4 || confirmPassword.count != 4 {
            self.showBanner(title: "Alert", subTitle: "Please enter 4 digit smart pass" , style: .warning)
            return
            
        }
        
        if password != confirmPassword {
            self.showBanner(title: "Alert", subTitle: "New smart pass & confirm pass does not match" , style: .danger)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["teacherId": id , "oldSmartPass" : oldPassword , "newSmartPass": password]
        print(params)
        
        ProfileServices.UpdateSmartPass(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.navigationController?.isNavigationBarHidden = true
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
}

extension ChangeSmartPassViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let char = string.cString(using: String.Encoding.utf8)
        let isBackSpace = strcmp(char, "\\b")
        if isBackSpace == -92 {
            return true
        }
        
        return textField.text!.count <= 3
        
    }
    
}

