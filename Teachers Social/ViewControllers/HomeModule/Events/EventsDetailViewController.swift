//
//  EventsDetailViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 26/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension EventsDetailViewController : AlertViewDelegate , SuccessViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        if self.textAlert == "Terms" {
            self.termsView.isHidden = true
            return
        }
        self.navigationController?.popViewController(animated: true)
        self.successView.isHidden = true
    }
    
    
}

class EventsDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> EventsDetailViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EventsDetailViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    // Top View
    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblTopDate:UILabel!
    @IBOutlet weak var lblTopPrice:UILabel!
    
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblEventType:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblPeople:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var lblCount:UILabel!
    
    var eventObj:EventsList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.LightStatusBar()
        self.initData()
        self.eventDetails()
        
    }
    
    func initData() {
        
        if let image = eventObj!.eventPictureUrl {
            self.imgBanner.setImageFromUrl(urlStr: image)
        }
        
        if let name = eventObj!.title {
            self.lblTitle.text = name
        }
        
        if let category = eventObj!.eventCategory {
            self.lblCategory.text = category
            self.lblEventType.text = category.uppercased()
        }
        
        if let price = eventObj!.price {
            self.lblTopPrice.text = "\(price) AED per person"
            self.lblPrice.text = "\(price) AED per person".uppercased()
        }
        
        if let date = eventObj!.eventTimestamp {
            
            self.lblTopDate.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "dd-MM- yyyy")
            self.lblDate.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "dd-MM- yyyy")
            
            self.lblTime.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            
        }
        
        if let description = eventObj!.descriptionValue {
            self.lblDescription.text = description
        }
        
        if let people = eventObj!.attendees {
            self.lblPeople.text = "\(eventObj!.attendanceCapacity!) PERSON"
            self.lblCount.text = "\(people.count)"
        }
        
        if let location = eventObj!.eventLocation {
            self.lblLocation.text = location.uppercased()
        }
        
    }
    
    func eventDetails() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = self.eventObj!.id
        
        let param:[String:Any] = ["eventId":id!]
        print(param)
        
        EventServices.GetEventDetails(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
        })
        
    }
    
    @IBAction func bookNowAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let eventId = self.eventObj!.id
        let id = Singleton.sharedInstance.CurrentUser!.id
        
        let param:[String:Any] = ["teacherId": id! , "eventId":eventId!]
        print(param)
        
        EventServices.BookNowEvent(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                    self.textAlert = "Terms"
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                    self.textAlert = "Terms"
                }
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
    
}

extension EventsDetailViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![0] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 0
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
