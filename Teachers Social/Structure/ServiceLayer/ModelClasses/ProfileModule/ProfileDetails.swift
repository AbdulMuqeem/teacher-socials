//
//  ProfileDetails.swift
//
//  Created by Abdul Muqeem on 06/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ProfileDetails: NSCoding {

      // MARK: Declaration for string constants to be used to decode and also serialize.
      private struct SerializationKeys {
        static let name = "name"
        static let countryOfOrigin = "countryOfOrigin"
        static let emailId = "emailId"
        static let gender = "gender"
        static let profilePicture = "profilePicture"
        static let dateOfBirth = "dateOfBirth"
        static let contactNumber = "contactNumber"
      }

      // MARK: Properties
      public var name: String?
      public var countryOfOrigin: String?
      public var emailId: String?
      public var gender: Int?
      public var profilePicture: String?
      public var dateOfBirth: String?
      public var contactNumber: String?

      // MARK: SwiftyJSON Initializers
      /// Initiates the instance based on the object.
      ///
      /// - parameter object: The object of either Dictionary or Array kind that was passed.
      /// - returns: An initialized instance of the class.
      public convenience init(object: Any) {
        self.init(json: JSON(object))
      }

      /// Initiates the instance based on the JSON that was passed.
      ///
      /// - parameter json: JSON object from SwiftyJSON.
      public required init(json: JSON) {
        name = json[SerializationKeys.name].string
        countryOfOrigin = json[SerializationKeys.countryOfOrigin].string
        emailId = json[SerializationKeys.emailId].string
        gender = json[SerializationKeys.gender].int
        profilePicture = json[SerializationKeys.profilePicture].string
        dateOfBirth = json[SerializationKeys.dateOfBirth].string
        contactNumber = json[SerializationKeys.contactNumber].string
      }

      /// Generates description of the object in the form of a NSDictionary.
      ///
      /// - returns: A Key value pair containing all valid values in the object.
      public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = countryOfOrigin { dictionary[SerializationKeys.countryOfOrigin] = value }
        if let value = emailId { dictionary[SerializationKeys.emailId] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
        if let value = dateOfBirth { dictionary[SerializationKeys.dateOfBirth] = value }
        if let value = contactNumber { dictionary[SerializationKeys.contactNumber] = value }
        return dictionary
      }

      // MARK: NSCoding Protocol
      required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.countryOfOrigin = aDecoder.decodeObject(forKey: SerializationKeys.countryOfOrigin) as? String
        self.emailId = aDecoder.decodeObject(forKey: SerializationKeys.emailId) as? String
        self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
        self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
        self.dateOfBirth = aDecoder.decodeObject(forKey: SerializationKeys.dateOfBirth) as? String
        self.contactNumber = aDecoder.decodeObject(forKey: SerializationKeys.contactNumber) as? String
      }

      public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(countryOfOrigin, forKey: SerializationKeys.countryOfOrigin)
        aCoder.encode(emailId, forKey: SerializationKeys.emailId)
        aCoder.encode(gender, forKey: SerializationKeys.gender)
        aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
        aCoder.encode(dateOfBirth, forKey: SerializationKeys.dateOfBirth)
        aCoder.encode(contactNumber, forKey: SerializationKeys.contactNumber)
      }

    }
