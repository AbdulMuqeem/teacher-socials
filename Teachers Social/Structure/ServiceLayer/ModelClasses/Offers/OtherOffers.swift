//
//  OtherOffers.swift
//
//  Created by Abdul Muqeem on 17/12/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class OtherOffers: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isFree = "isFree"
    static let offerDiscount = "offerDiscount"
    static let offerName = "offerName"
    static let id = "_id"
    static let offerPictureUrl = "offerPictureUrl"
  }

  // MARK: Properties
  public var isFree: Bool? = false
  public var offerDiscount: String?
  public var offerName: String?
  public var id: String?
  public var offerPictureUrl: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isFree = json[SerializationKeys.isFree].boolValue
    offerDiscount = json[SerializationKeys.offerDiscount].string
    offerName = json[SerializationKeys.offerName].string
    id = json[SerializationKeys.id].string
    offerPictureUrl = json[SerializationKeys.offerPictureUrl].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.isFree] = isFree
    if let value = offerDiscount { dictionary[SerializationKeys.offerDiscount] = value }
    if let value = offerName { dictionary[SerializationKeys.offerName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = offerPictureUrl { dictionary[SerializationKeys.offerPictureUrl] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isFree = aDecoder.decodeBool(forKey: SerializationKeys.isFree)
    self.offerDiscount = aDecoder.decodeObject(forKey: SerializationKeys.offerDiscount) as? String
    self.offerName = aDecoder.decodeObject(forKey: SerializationKeys.offerName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.offerPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.offerPictureUrl) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isFree, forKey: SerializationKeys.isFree)
    aCoder.encode(offerDiscount, forKey: SerializationKeys.offerDiscount)
    aCoder.encode(offerName, forKey: SerializationKeys.offerName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(offerPictureUrl, forKey: SerializationKeys.offerPictureUrl)
  }

}
