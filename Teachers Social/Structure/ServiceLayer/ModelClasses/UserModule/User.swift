//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {

      // MARK: Declaration for string constants to be used to decode and also serialize.
      private struct SerializationKeys {
        static let jwtToken = "jwtToken"
        static let name = "name"
        static let id = "_id"
        static let emailId = "emailId"
        static let profilePicture = "profilePicture"
      }

      // MARK: Properties
      public var jwtToken: String?
      public var name: String?
      public var id: String?
      public var emailId: String?
      public var profilePicture: String?

      // MARK: SwiftyJSON Initializers
      /// Initiates the instance based on the object.
      ///
      /// - parameter object: The object of either Dictionary or Array kind that was passed.
      /// - returns: An initialized instance of the class.
      public convenience init(object: Any) {
        self.init(json: JSON(object))
      }

      /// Initiates the instance based on the JSON that was passed.
      ///
      /// - parameter json: JSON object from SwiftyJSON.
      public required init(json: JSON) {
        jwtToken = json[SerializationKeys.jwtToken].string
        name = json[SerializationKeys.name].string
        id = json[SerializationKeys.id].string
        emailId = json[SerializationKeys.emailId].string
        profilePicture = json[SerializationKeys.profilePicture].string
      }

      /// Generates description of the object in the form of a NSDictionary.
      ///
      /// - returns: A Key value pair containing all valid values in the object.
      public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = jwtToken { dictionary[SerializationKeys.jwtToken] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = emailId { dictionary[SerializationKeys.emailId] = value }
        if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
        return dictionary
      }

      // MARK: NSCoding Protocol
      required public init(coder aDecoder: NSCoder) {
        self.jwtToken = aDecoder.decodeObject(forKey: SerializationKeys.jwtToken) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.emailId = aDecoder.decodeObject(forKey: SerializationKeys.emailId) as? String
        self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
      }

      public func encode(with aCoder: NSCoder) {
        aCoder.encode(jwtToken, forKey: SerializationKeys.jwtToken)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(emailId, forKey: SerializationKeys.emailId)
        aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
        
      }

    }
