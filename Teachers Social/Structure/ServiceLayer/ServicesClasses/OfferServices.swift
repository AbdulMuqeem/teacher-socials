//
//  OfferServices.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 26/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class OfferServices {
    
    static func GetOfferCategoryList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.offerCategoryList , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetOfferList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.offerList , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func GetSearchOffer(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.searchOffer , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetOfferDetails(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.offerDetails , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func BookOffers(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.bookOffer , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
}
