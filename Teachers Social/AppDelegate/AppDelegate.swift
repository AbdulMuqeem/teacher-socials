//
//  AppDelegate.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 30/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        SocialManager.GoogleAuth(clientID: GOOGLE_CLIENT_ID)
        SocialManager.EnableFacebook(application: application, launchOptions: launchOptions)
        
        IQKeyboardManager.shared.enable = true
        self.NavigateTOInitialViewController()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return SocialManager.URLScheme(app: app, open: url, options: options)        
    }
    
    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
                
       if UserManager.isUserLogin() {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = HomeOffersViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
        
        } else {
    
            let check = UserDefaults.standard.object(forKey: "isFirstLaunched") as? String
        
            if check == "true" {
                let nav = RootViewController.instantiateFromStoryboard()
                self.window?.rootViewController = nav
                let vc = LoginViewController.instantiateFromStoryboard()
                nav.pushViewController(vc, animated: true)
            }
            else {
                let nav = RootViewController.instantiateFromStoryboard()
                self.window?.rootViewController = nav
                let vc = TutorialViewController.instantiateFromStoryboard()
                nav.pushViewController(vc, animated: true)
            }
        }
    }
}
    

